from distutils.core import setup
setup(
  name = 'onemarket',
  packages = ['onemarket'],
  url='https://bitbucket.org/onemarkettech/onemarket-api/',
  download_url='https://bitbucket.org/onemarkettech/onemarket-api/get/0.5.tar.gz',
  version = '0.5',
  description = 'An API for the Onemarket platform',
  author='Vasil Vasilev',
  author_email='vasil@onemarket.tech',
  license='Apache 2.0',
  zip_safe=False
)
